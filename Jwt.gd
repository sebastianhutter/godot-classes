extends Node
class_name Jwt

#
# parse jwt tokens
# validate signature against given jwk
#


##
# export vars
##

##
# script vars
##

var jwks : Dictionary
var jwks_url : String

##
# signals
##

signal jwks_received
signal jwks_ready

##
# default functions
##

func _init(url: String = "") -> void:
	# initialize jwks urls 
	# if no url is set no jwks will be downloaded
	# and parsed

	jwks_url = url

func _ready() -> void:
	# download jwks and parse certificates
	# and initialize download loop to regularly
	# update the jwks

	Logger.debug(self, "ready function started")
	if jwks_url:
		self.connect("jwks_received", self, "_parse_rsa_public_keys")
		_get_jwks()

		# enter an infinite loop to update the jwks regularly
		while true:
			yield(get_tree().create_timer(600.0), "timeout")
			_get_jwks()

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _http_request_completed_jwks(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray) -> void:	
	# parse downloaded jwks json

	var j : JSONParseResult = JSON.parse(body.get_string_from_utf8())
	if j.error != OK:
		Logger.error(self, "Invalid json received")
	jwks = j.result
		
	emit_signal("jwks_received")

##
# script functions
##

func _get_jwks() -> void:
	# download jwks

	Logger.debug(self, "execute _get_jwks")
	# retrieve the jwks config
	var http = HTTPRequest.new()
	add_child(http, true)
	http.connect("request_completed", self, "_http_request_completed_jwks")
	http.request(jwks_url)
	yield(http,"request_completed")
	remove_child(http)

func _parse_rsa_public_keys() -> void:
	# parse all rsa sig public keys
	# we need to use openssl for this as godot doesnt make it easy to
	# get the public key from the jwk

	if not jwks.has("keys"):
		Logger.error(self, "Invalid jwks json")
		return
	
	var file = File.new()
	for k in jwks["keys"]:
		var kid : String = k["kid"]
		var alg : String = k["alg"]
		var kty : String = k["kty"]
		var use : String = k["use"]
		var x5c : String = k["x5c"][0]

		Logger.debug(self, "Parse x5c for kid {kid}".format({"kid":kid}))
		if alg == "RS256" and kty == "RSA" and use == "sig":
			# create "certificate" from given x5c
			# write file to filesystem to parse with openssl
			# get public key from certificate
			# remove certificate
			
			var fname : String = "user://{kid}_{ts}".format({"kid": kid, "ts": OS.get_unix_time()})
			var cert : String = "-----BEGIN CERTIFICATE-----\n{x5c}\n-----END CERTIFICATE-----".format({"x5c": x5c})
			file.open(fname, File.WRITE)
			file.store_string(cert)
			var cpath : String = file.get_path_absolute()
			file.close()
			
			var output : Array = []
			var exit_code = OS.execute("openssl", ["x509", "-in", cpath, "-pubkey", "-noout"], true, output)
			if exit_code != 0:
				Logger.error(self, "Unable to parse jwk certificate for {kid}".format({"kid":kid}))

			var d : Directory = Directory.new()
			d.remove(fname)

			var key = CryptoKey.new()
			key.load_from_string(output[0],true)
			k["pubkey"] = key
			
	emit_signal("jwks_ready")
	
func _base64url_to_base64(t: String) -> String:
	# convert base64url to base64

	var n : String
	n = t.replace("-", "+").replace("_", "/")
	var mod : int = n.length() % 4
	if mod > 0:
		for i in range(4 - mod):
			n += "="
	
	return n

func _base64_to_base64url(t: String) -> String:
	# convert base64 to base64url
	
	var n : String
	n = t.replace("+", "-").replace("/", "_").replace("=", "")

	return n

func _split_token(token: String) -> PoolStringArray:
	# take the given jwt token and split it into header, payload and signature

	var t : PoolStringArray = token.split(".")
	if len(t) != 3:
		Logger.warning(self, "Invalid token structure")
	return t

func _decode_header(token: String) -> JSONParseResult:
	# return the decoded jwt header from the jwt token

	var t : PoolStringArray = _split_token(token)
	var b : String = _base64url_to_base64(t[0])
	return JSON.parse(Marshalls.base64_to_utf8(b))

func _decode_payload(token: String) -> JSONParseResult:
	# return the decoded jwt payload from the jwt token

	var t : PoolStringArray = _split_token(token)
	var b : String = _base64url_to_base64(t[1])
	return JSON.parse(Marshalls.base64_to_utf8(b))

func _decode_signature(token: String) -> PoolByteArray:
	# return the decoded jwt signature from the jwt token

	var t : PoolStringArray = _split_token(token)
	var b : String = _base64url_to_base64(t[2])
	return Marshalls.base64_to_raw(b)

func get_header_from_token(token: String) -> String:
	# return the encoded header from the jwt token

	var t = _split_token(token)
	if len(t) != 3:
		Logger.warning(self, "Invalid token structure")
		return ""

	return t[0]

func get_payload_from_token(token: String) -> String:
	# return the encoded payload from the jwt token
	
	var t = _split_token(token)
	if len(t) != 3:
		Logger.warning(self, "Invalid token structure")
		return ""

	return t[1]

func get_signature_from_token(token: String) -> String:
	# return the encoded signature from the jwt token

	var t = _split_token(token)
	if len(t) != 3:
		Logger.warning(self, "Invalid token structure")
		return ""

	return t[2]

func get_value_from_header(token: String, key: String) -> String:
	# return a value from the header of a given jwt token

	var h : JSONParseResult = _decode_header(token)
	if h.error != OK:
		Logger.warning(self, "Invalid header")
		return ""
	
	if h.result.has(key):
		return h.result[key]
	else:
		Logger.warning(self, "Key {k} not found in header".format({"k":key}))
	
	return ""
	
func get_value_from_payload(token: String, key: String) -> String:
	# return a value from the payload of the given jwt token
	# the function converts any value to a string
	# arrays are comma joined

	var p : JSONParseResult = _decode_payload(token)
	if p.error != OK:
		Logger.warning(self, "Invalid header")
		return ""

	if p.result.has(key):
		var v = p.result[key]
		if typeof(v) == TYPE_ARRAY:
			var pv : PoolStringArray = v
			return pv.join(",")
		else:
			return v
	else:
		Logger.warning(self, "Key {k} not found in payload".format({"k":key}))
	
	return ""

func verify_token(token: String, issuer: String = "", audience: String = "", verify_expiration: bool = true, verify_issuer: bool = true, verify_audience: bool = true, verify_signature: bool = true) -> bool:
	# verify the given access or id token
	# https://auth0.com/docs/security/tokens/json-web-tokens/validate-json-web-tokens

	if _decode_header(token).error != OK:
		Logger.warning(self, "Invalid header")
		return false
	if _decode_payload(token).error != OK:
		Logger.warning(self, "Invalid payload")
		return false
	if _decode_signature(token).size() <= 0:
		Logger.warning(self, "Invalid signature")
		return false
		
	# check expiration
	if verify_expiration:
		if int(get_value_from_payload(token, "exp"))< OS.get_unix_time():
			Logger.warning(self, "Token has expired")
			return false

	# check issuer
	if verify_issuer and issuer:
		if get_value_from_payload(token, "iss") != issuer:
			Logger.warning(self, "Invalid issuer")
			return false

	# check audience
	if verify_audience and audience:
		var a : Array = get_value_from_payload(token, "aud").split(",")
		if not a.has(audience):
			Logger.warning(self, "Invalid audience")
			return false

	# verify signature
	if verify_signature:
		var kid = get_value_from_header(token, "kid")
		if kid == "":
			Logger.warning(self, "Unable to retrieve kid")
			return false
			
		for k in jwks["keys"]:
			if k["kid"] == kid:
				# lets recreate the base64url encoded values
				# this verifies our base64 functions work as expected
				var calculated_header : String = _base64_to_base64url(Marshalls.utf8_to_base64(JSON.print(_decode_header(token).result)))
				var calculated_payload : String = _base64_to_base64url(Marshalls.utf8_to_base64(JSON.print(_decode_payload(token).result)))
				var calculated_signature : String = "{h}.{p}".format({"h":calculated_header, "p":calculated_payload})
				# verify signature against our calculated signature
				var crypto = Crypto.new()
				if not crypto.verify(HashingContext.HASH_SHA256, calculated_signature.sha256_buffer(), _decode_signature(token), k["pubkey"]):
					Logger.warning(self, "Invalid signature")
					return false
	
	return true
