extends Node
class_name Rpc

#
# logger class to help with outputs
#

##
# export vars
##

##
# script vars
##

enum CODE {
	# all went well
	OK = 0,
	
	# connection errors
	CONNECTION_FAILED = 11

	# authentication errors
	REGISTRATION_FAILED = 21
	LOGIN_FAILED = 22
	INVALID_TOKEN = 23
	AUTHENTICATION_FAILED = 24
	
	# database operation errors
	DATABASE_UPSERT_FAILED = 31
	DATABSE_GET_FAILED = 32
}

const MSG : Dictionary = {
	# CONNECTION_FAILED
	11: "Unable to connect to game server. Please try again later.",
	# REGISTRATION_FAILED:
	21: "Registration failed. Please verify username, password and email address.",
	# LOGIN_FAILED
	22: "Login to game server failed. Pleasew try again later.",
	# INVALID_TOKEN
	23: "Invalid authentication token received. Please try again later.",
	# AUTHENTICATION_FAILED
	24: "Authentication failed. Please check your username and password.",
	# DATABASE_UPSERT_FAILED
	31: "Unable to update database. Please try again later.",
	# DATABSE_GET_FAILED
	32: "Unable to load database. Please try again later.",
}

##
# signals
##

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

static func message(code: int) -> String:
	# return an error message matching the given rpc error code
	if MSG.has(code):
		return MSG[code]
	return "Unknown RPC error {code} received".format({"code":code})
