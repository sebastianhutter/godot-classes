extends Node
class_name Auth0

#
# auth0 class to register, login and retrieve access tokens from
# auth0
#


##
# export vars
##

##
# script vars
##

# required auth0 fields
var domain : String
var client_id : String
var client_secret: String
# optional auth0 fields
var audience : String
var scope : String
var database_connection : String

# store openid config and jwt handler
var jwt : Jwt
var openId : OpenId

##
# signals
##

signal auth0_ready
signal user_registration_succeeded
signal user_registration_failed
signal user_login_succeeded
signal user_login_failed
signal machine_login_succeeded
signal machine_login_failed

##
# default functions
##

func _init(d: String, cid: String = "", csec: String = "")  -> void:
	# intialize auth0 class
	# if no client id and client secret are specified
	# no authentication will be possible
	
	domain = d
	client_id = cid
	client_secret = csec

func _ready() -> void:
	# setup auth0 openid configuration and jwk 

	Logger.debug(self, "ready function started")

	# download auth0 openid configuration
	openId = OpenId.new("https://{domain}/.well-known/openid-configuration".format({"domain": domain}))
	add_child(openId)
	yield(openId, "openid_configuration_ready")

	# initialize jwks
	jwt = Jwt.new(openId.jwks_uri)
	add_child(jwt)
	yield(jwt,"jwks_ready")
	
	# with the openid config and the jwks ready lets tell the 
	# gateway it can finalize its initialization
	emit_signal("auth0_ready")

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _http_request_completed_user(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray, type: String, rpc_id: int) -> void:
	# validate the response and send signals

	# parse response, auth0 always sends back a valid response
	# except of course no connection could be established
	Logger.debug(self, "received response from auth0 (code {c})".format({"c":response_code}))
	var sig_failed : String
	var sig_success
	if type == "register":
		sig_failed = "user_registration_failed" 
		sig_success = "user_registration_succeeded" 
	elif type == "login":
		sig_failed = "user_login_failed" 
		sig_success = "user_login_succeeded" 

	var j : JSONParseResult = JSON.parse(body.get_string_from_utf8())
	if j.error != OK:
		Logger.error(self, "Invalid json received")
		emit_signal(sig_failed, {}, rpc_id)
		return

	var response : Dictionary = j.result
	# if no valid response is recieved send a failed signal
	if response_code != 200:	 
		if "error" in response:
			Logger.error(self, response["error"])
		elif "code" in response:
			Logger.error(self, response["code"])
		else:
			Logger.error(self, "Unknown error occured with auth0 request")
		emit_signal(sig_failed, {}, rpc_id)
		return


	# if we got a valid response we should have an access and id token
	# lets verify them to be double safe
	if type == "login":
		Logger.debug(self, "verify access token")
		if not jwt.verify_token(response["access_token"], openId.issuer, audience):
			Logger.warning(self, "Invalid access token")
			emit_signal(sig_failed, {}, rpc_id)
			return
		
		Logger.debug(self, "verify id token")
		if not jwt.verify_token(response["id_token"], openId.issuer, "", true, true, false, true):
			Logger.warning(self, "Invalid access token")
			emit_signal(sig_failed, {}, rpc_id)
			return

	# if verification has passed send the complete signal
	emit_signal(sig_success, response, rpc_id)

func _http_request_completed_machine(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray) -> void:
	# validate the response and send signals

	# parse response, auth0 always sends back a valid response
	# except of course no connection could be established
	Logger.debug(self, "received response from auth0 (code {c})".format({"c":response_code}))

	var j : JSONParseResult = JSON.parse(body.get_string_from_utf8())
	if j.error != OK:
		Logger.error(self, "Invalid json received")
		emit_signal("machine_login_failed", {})
		return

	var response : Dictionary = j.result
	# if no valid response is recieved send a failed signal
	if response_code != 200:	 
		if "error" in response:
			Logger.error(self, response["error"])
		elif "code" in response:
			Logger.error(self, response["code"])
		else:
			Logger.error(self, "Unknown error occured with auth0 request")
		emit_signal("machine_login_failed", {})
		return

	# if we got a valid response we should have an access and id token
	# lets verify them to be double safe
	Logger.debug(self, "verify access token")
	if not jwt.verify_token(response["access_token"], openId.issuer, audience):
		Logger.warning(self, "Invalid access token")
		emit_signal("machine_login_failed", {})
		return

	# if verification has passed send the complete signal
	emit_signal("machine_login_succeeded", response)


##
# script functions
##


func register_user(username: String, email: String, password: String, rpc_id: int) -> void:
	# register a user with username, password and email address
	
	var url : String = "https://{domain}/dbconnections/signup".format({"domain": domain})
	Logger.debug(self, "calling {url} with username {user}".format({"url":url, "user":username}))
	var payload : Dictionary = {
		"client_id": client_id,
		"username": username,
		"nickname": username,
		"email": email,
		"password": password,
		"connection": database_connection
	}
	
	var http = HTTPRequest.new()
	add_child(http,true)
	http.connect("request_completed", self, "_http_request_completed_user", ["register", rpc_id])
	
	var error = http.request(
		url,
		["Content-Type: application/json"],
		true,
		HTTPClient.METHOD_POST,
		JSON.print(payload)
	)
	if error != OK:
		push_error("An error occurred in the HTTP request.")
	
	# remove http client after request is completed
	yield(http,"request_completed")
	remove_child(http)

func login_user(username: String, password: String, rpc_id: int) -> void:
	# login user with given username and password
	
	var url : String = openId.token_endpoint
	Logger.debug(self, "calling {url} with username {user} with rpc id {id}".format({"url":url, "user":username, "id": str(rpc_id)}))
	var payload : PoolStringArray = [
		"grant_type=password",
		"username={u}".format({"u": username}),
		"password={p}".format({"p": password}),
		"audience={a}".format({"a": audience}),
		"scope={s}".format({"s": scope}),
		"client_id={c}".format({"c": client_id}),
		"client_secret={s}".format({"s": client_secret}),
		"response_type=code+id_token"
	]

	var http = HTTPRequest.new()
	add_child(http,true)
	http.connect("request_completed", self, "_http_request_completed_user", ["login", rpc_id])

	var error = http.request(
		url,
		["Content-Type: application/x-www-form-urlencoded"],
		true,
		HTTPClient.METHOD_POST,
		payload.join("&")
	)
	if error != OK:
		push_error("An error occurred in the HTTP request.")

	# remove http client after request is completed
	yield(http,"request_completed")
	remove_child(http)

func login_machine2machine() -> void:
	# log into auth0 with client credentials grant

	var url : String = openId.token_endpoint
	Logger.debug(self, "calling {url} machine2machine".format({"url": url}))
	var payload : PoolStringArray = [
		"grant_type=client_credentials",
		"audience={a}".format({"a": audience}),
		"client_id={c}".format({"c": client_id}),
		"client_secret={s}".format({"s": client_secret}),
		"scope={s}".format({"s": scope}),
	]

	var http = HTTPRequest.new()
	add_child(http,true)
	http.connect("request_completed", self, "_http_request_completed_machine")

	var error = http.request(
		url,
		["Content-Type: application/x-www-form-urlencoded"],
		true,
		HTTPClient.METHOD_POST,
		payload.join("&")
	)
	if error != OK:
		push_error("An error occurred in the HTTP request.")

	# remove http client after request is completed
	yield(http,"request_completed")
	remove_child(http)
