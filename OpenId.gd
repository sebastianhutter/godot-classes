extends Node
class_name OpenId

#
# download and parse openid configuration from a well-known source
#


##
# export vars
##

##
# script vars
##

# url to well knowns id
var _well_knowns_url : String

# dictionary containing the unparsed openid config
var _openid : Dictionary
# variabls for parsed well known json
var issuer : String
var authorization_endpoint : String
var token_endpoint : String
var device_authorization_endpoint : String
var userinfo_endpoint : String
var mfa_challenge_endpoint : String
var jwks_uri : String
var registration_endpoint : String
var revocation_endpoint : String
var scopes_supported : PoolStringArray
var response_types_supported : PoolStringArray
var code_challenge_methods_supported : PoolStringArray
var response_modes_supported : PoolStringArray
var subject_types_supported : PoolStringArray
var id_token_signing_alg_values_supported : PoolStringArray
var token_endpoint_auth_methods_supported : PoolStringArray
var claims_supported : PoolStringArray
var request_uri_parameter_supported : bool

##
# signals
##

signal openid_configuration_received
signal openid_configuration_ready

##
# default functions
##

func _init(u: String) -> void:
	# intialize openid object
	_well_knowns_url = u

func _ready() -> void:
	# download and parse openid project
	# and initialize reload loop to regularly update
	# the openid configuration

	Logger.debug(self, "ready function started")

	# download auth0 openid configuration
	_get_openid_configuration()
	yield(self, "openid_configuration_received")

	# parse openid configuration
	_parse_openid_configuration()
	emit_signal("openid_configuration_ready")
	
	# enter an infinite loop to update the openid config and jwks regularly
	while true:
		yield(get_tree().create_timer(600.0), "timeout")
		_get_openid_configuration()
		yield(self, "openid_configuration_received")
		_parse_openid_configuration()


func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _http_request_completed_openid_configuration(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray) -> void:	
	# parse the downloaded data as json

	var j : JSONParseResult = JSON.parse(body.get_string_from_utf8())
	if j.error != OK:
		push_error("Invalid json received")

	_openid = j.result
	emit_signal("openid_configuration_received")

##
# script functions
##

func _get_openid_configuration() -> void:
	# retrieve the openid configuration from the oauth auto config url

	Logger.debug(self, "execute _get_openid_configuration")
	var http = HTTPRequest.new()
	add_child(http, true)
	http.connect("request_completed", self, "_http_request_completed_openid_configuration")
	http.request(_well_knowns_url)
	yield(http,"request_completed")
	remove_child(http)

func _parse_openid_configuration() -> void:
	# parse the openid configuration

	for k in _openid:
		var v = _openid[k]
		Logger.debug(self, "Assign {k} with {v}".format({"k":k, "v":v}))
		if k == "issuer":
			issuer = v
		elif k == "authorization_endpoint":
			authorization_endpoint = v
		elif k == "token_endpoint":
			token_endpoint = v
		elif k == "device_authorization_endpoint":
			device_authorization_endpoint = v
		elif k == "userinfo_endpoint":
			userinfo_endpoint = v
		elif k == "mfa_challenge_endpoint":
			mfa_challenge_endpoint = v
		elif k == "jwks_uri":
			jwks_uri = v
		elif k == "registration_endpoint":
			registration_endpoint = v
		elif k == "revocation_endpoint":
			revocation_endpoint = v
		elif k == "scopes_supported":
			scopes_supported = v
		elif k == "response_types_supported":
			response_types_supported = v
		elif k == "code_challenge_methods_supported":
			code_challenge_methods_supported = v
		elif k == "response_modes_supported":
			response_modes_supported = v
		elif k == "subject_types_supported":
			subject_types_supported = v
		elif k == "id_token_signing_alg_values_supported":
			id_token_signing_alg_values_supported = v
		elif k == "token_endpoint_auth_methods_supported":
			token_endpoint_auth_methods_supported = v
		elif k == "claims_supported":
			claims_supported = v
		elif k == "request_uri_parameter_supported":
			request_uri_parameter_supported = v
		else:
			Logger.warning(self, "Unknown openid key {k}".format({"k":k}))
