extends Node
class_name Logger

#
# logger class to help with outputs
#

##
# export vars
##

##
# script vars
##

enum LOGLEVEL { 
	DEBUG, # 0
	INFO,  # 1
	WARNING, # 2
	ERROR, # 3
}

##
# signals
##

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

static func get_loglevel(loglevel) -> int:
	# parse the given loglevel and return a matching LOGLEVEL
	
	var lvl : int
	if typeof(loglevel) == TYPE_INT:
		if loglevel >= 0 or loglevel <=3:
			lvl = loglevel
		else:
			push_warning("Given loglevel (int) invalid")
			lvl = 1
	elif typeof(loglevel) == TYPE_STRING:
		if loglevel.to_lower() == "debug":
			lvl = 0
		elif loglevel.to_lower() == "info":
			lvl = 1
		elif loglevel.to_lower() == "warning":
			lvl = 2
		elif loglevel.to_lower() == "error":
			lvl = 3
		else:
			push_warning("Given loglevel (string) invalid")
			lvl = 1
	else:
		# if unknown return INFO loglevel
		push_warning("Unknown loglevel type received")
		lvl = 1
	return lvl


static func debug(caller: Node, msg: String) -> void:
	# print debug messages

	if Config.app["loglevel"] == LOGLEVEL.DEBUG:
		var dt : Dictionary = OS.get_datetime(false)
		var timestamp : String = "{Y}/{M}/{D} {h}:{m}:{s}".format({
			"Y": dt["year"],
			"M": dt["month"],
			"D": dt["day"],
			"h": dt["hour"],
			"m": dt["minute"],
			"s": dt["second"],
		})
		
		print("{timestamp}:DEBUG - {caller}: {msg}".format({
			"timestamp": timestamp,
			"caller": caller.get_script().get_path(),
			"msg": msg,
		}))

static func info(caller: Node, msg: String) -> void:
	# print info messages

	if (Config.app["loglevel"] == LOGLEVEL.DEBUG or
		Config.app["loglevel"] == LOGLEVEL.INFO):
		var dt : Dictionary = OS.get_datetime(false)
		var timestamp : String = "{Y}/{M}/{D} {h}:{m}:{s}".format({
			"Y": dt["year"],
			"M": dt["month"],
			"D": dt["day"],
			"h": dt["hour"],
			"m": dt["minute"],
			"s": dt["second"],
		})
		
		print("{timestamp}:INFO - {caller}: {msg}".format({
			"timestamp": timestamp,
			"caller": caller.get_script().get_path(),
			"msg": msg,
		}))

static func warning(caller: Node, msg: String) -> void:
	# print warning messages

	if (Config.app["loglevel"] == LOGLEVEL.DEBUG or
		Config.app["loglevel"] == LOGLEVEL.INFO or
		Config.app["loglevel"] == LOGLEVEL.WARNING):
		var dt : Dictionary = OS.get_datetime(false)
		var timestamp : String = "{Y}/{M}/{D} {h}:{m}:{s}".format({
			"Y": dt["year"],
			"M": dt["month"],
			"D": dt["day"],
			"h": dt["hour"],
			"m": dt["minute"],
			"s": dt["second"],
		})
		
		print("{timestamp}:WARNING - {caller}: {msg}".format({
			"timestamp": timestamp,
			"caller": caller.get_script().get_path(),
			"msg": msg,
		}))

static func error(caller: Node, msg: String) -> void:
	# print error messages
	
	if (Config.app["loglevel"] == LOGLEVEL.DEBUG or
		Config.app["loglevel"] == LOGLEVEL.INFO or
		Config.app["loglevel"] == LOGLEVEL.WARNING or
		Config.app["loglevel"] == LOGLEVEL.ERROR):
		var dt : Dictionary = OS.get_datetime(false)
		var timestamp : String = "{Y}/{M}/{D} {h}:{m}:{s}".format({
			"Y": dt["year"],
			"M": dt["month"],
			"D": dt["day"],
			"h": dt["hour"],
			"m": dt["minute"],
			"s": dt["second"],
		})
		
		print("{timestamp}:ERROR - {caller}: {msg}".format({
			"timestamp": timestamp,
			"caller": caller.get_script().get_path(),
			"msg": msg,
		}))
