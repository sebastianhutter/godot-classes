# Godot Classes

Godot Classes to be shared and reused

## Classes
- Auth0.gd - Wrapper for auth0 login and registration
- DotEnv.gd - Parse .env files and helper functions to load env variables into varians
- Jwt.gd - Parse and verify jwt tokens (requires `openssl` for public key verification!)
- Logger.gd - Simple logger class
- OpenId.gd - Download and parse [openid connect disocvery documents](https://swagger.io/docs/specification/authentication/openid-connect-discovery/)
- Rpc.gd - Contains shared rpc error codes and messages
## Use as submodule

```bash
# add the classes submodule into the folder "Classes"
git submodule add https://gitlab.com/sebastianhutter/godot-classes.git Classes
```

