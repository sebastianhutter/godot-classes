extends Node
class_name DotEnv

#
# helper functions to retrieve configuration values from 
# environment variables
#


##
# export vars
##

##
# script vars
##

##
# signals
##

##
# default functions
##

func _init() -> void:
	pass		

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

static func load(env: String = ".env") -> void:
	# parse the given .env file and load values into process environment

	var dir : Directory = Directory.new()
	var fp : File = File.new()
	var reg_result : RegExMatch 
	var reg_comment : RegEx = RegEx.new()
	var reg_entry : RegEx = RegEx.new()

	if not dir.file_exists(env):
		push_warning("Environment file {e} not found. Unable to parse it".format({"e":env}))
		return
	
	# first regexp checks if the line is a comment
	# second rexexp parses non comment lines and tries to split
	# it into key value pairs
	reg_comment.compile("^\\s{0,}#")
	reg_entry.compile("^(?<key>[\\d\\w-]+)=\"?(?<value>[\\s.\\?,/!\\:\\d;\\w-]{1,})\"?")

	fp.open(env, File.READ)
	while not fp.eof_reached():
		var line : String = fp.get_line()

		reg_result = reg_comment.search(line)
		if reg_result:
			continue

		reg_result = reg_entry.search(line)
		if reg_result:
			OS.set_environment(reg_result.get_string("key"), reg_result.get_string("value"))
	fp.close()

static func envString(env: String, default: String = "") -> String:
	# returns the given env var as string

	var v : String = OS.get_environment(env)
	
	if v == "":
		push_warning("Environment variable {e} not found. Returning default value".format({"e": env}))
		return default
	
	return v

static func envInt(env: String, default: int = 0) -> int:
	# returns the given env var as int
	
	var v : String = OS.get_environment(env)
	
	if v == "":
		push_warning("Environment variable {e} not found. Returning default value".format({"e": env}))
		return default
	
	return int(v)
